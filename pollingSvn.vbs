' SVNへのポーリング
' cscript //nologo pollingSvn.vbs  pollingSvnConfig.ini
Option Explicit

Dim pollingUrl, triggerBatch, acquisitionLogfile, evacuationLogfile  ' iniパラメータ
Dim objFileSys, objShell, objArgs

Set objArgs = WScript.Arguments
Set objFileSys = CreateObject("Scripting.FileSystemObject")

If objArgs.Count <> 1 Then
	'引数は必須
	WScript.Echo "設定ファイルを指定してください"
	WScript.Quit 1
End If
If (False = objFileSys.FileExists(objArgs(0))) Then
	'引数は必須
	WScript.Echo "設定ファイルを指定してください"
	WScript.Quit 1
End If

' 設定値読込
ExecuteGlobal objFileSys.OpenTextFile(objArgs(0)).ReadAll()

If (pollingUrl = "" Or acquisitionLogfile = "" Or evacuationLogfile = "") Then
	'環境設定は必須
	WScript.Echo "環境設定してください"
	WScript.Quit 1
End If

Set objShell = CreateObject("WScript.Shell")

Call pollingStart(pollingUrl, triggerBatch)

Set objShell = Nothing
Set objFileSys = Nothing
WScript.Quit 0

'--------------------------------------
'ポーリングの実行
' pollingUrl     SVNのURL
' triggerBatch   差分発生時のバッチファイル(フルパス, 引数は指定可能)
'
Sub pollingStart(pollingUrl, triggerBatch)

	Dim cdFolder, logNew, logOld, isDiff
	Dim pollingCmd, triggerCmd

	cdFolder = objFileSys.getParentFolderName(WScript.ScriptFullName) & "\"
	logNew = cdFolder & acquisitionLogfile
	logOld = cdFolder & evacuationLogfile

	pollingCmd = "cmd /C svn log " & pollingUrl & " --limit 2 > " & logNew
	triggerCmd = "cmd /C " & triggerBatch

	Call makeFile(logNew)
	Call makeFile(logOld)

	If pollingUrl <> "" Then
		' ポーリング
		objShell.Run pollingCmd, 0, True
	End If

	'比較
	isDiff = IsDiffFile(logNew, logOld)

	' 次回用に退避
	objFileSys.DeleteFile logOld, True
	objFileSys.MoveFile logNew, logOld

	If isDiff Then
		'差分あり
		If triggerBatch <> "" Then
			objShell.Run triggerCmd, 0, True
		End If
	End If

End Sub

'--------------------------------------
'ファイルの作成
' filePath       ファイルパス
'
Sub makeFile(filePath)
	If Not objFileSys.FileExists(filePath) Then
		' ファイルが無い場合は空で作成
		Call objFileSys.CreateTextFile(filePath)
	End If
End Sub

'--------------------------------------
'差分があればtrueを返す
' file1Path      ファイル1
' file2Path      ファイル2
'
Function IsDiffFile(file1Path, file2Path)

	Dim text1, text2, objFile1

	text1 = ""
	On Error Resume Next ' 強行
		Set objFile1 = objFileSys.OpenTextFile(file1Path, 1) 
		If Err.Number = 0 Then
			text1 = objFile1.ReadAll 
		End If
	On Error Goto 0 ' 解除

	text2 = ""
	On Error Resume Next ' 強行
		Set objFile1 = objFileSys.OpenTextFile(file2Path, 1) 
		If Err.Number = 0 Then
			text2 = objFile1.ReadAll
		End If
	On Error Goto 0 ' 解除


	objFile1.Close 
	If (text1 = "" And text2 = "") Then
		' 両方空
		IsDiffFile = False
	ElseIf InStr(text1,text2) = 0 Or InStr(text2,text1) = 0 Then 
		' 一致しない
		IsDiffFile = True
	Else
		'一致
		IsDiffFile = False
	End If 
End Function
