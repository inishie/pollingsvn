
**pollingSvn**

	SVNサーバへのポーリング

---

[TOC]

---

## Usage

	ポーリングの設定ファイルをvbsの引数に指定して起動します。
	SVNログに差が発生した場合、所定のコマンドを実行します。
	ex) cscript //nologo pollingSvn.vbs  pollingSvnConfig.ini

	ポーリングの設定は iniファイル で指定できます。
	ex) pollingSvnConfig.ini
		pollingUrl : SVNのURL ex: "http://127.0.0.1/repos/test/"
		triggerBatch : 差分発生時のバッチファイル ex: "updatesvn.bat"
		acquisitionLogfile : 取得時のログファイル名 ex: "svnLog-new.log"
		evacuationLogfile : 退避時のログファイル名 ex: "svnLog-old.log"


## Note

	・vbs, bat, ini は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・｢SVN別のコマンド｣を想定し、パラメータにファイルを指定する方式にしています。
	・ポーリングの実行端末が svn コマンド(commandline client tools) を利用します。


---

## References

	Tayack
	フォルダの差分を比較するためのVBScript - Qiita
	https://qiita.com/tayack/items/4900891ce2c65bf6253b

	@khsk
	Subversionのログをcronでポーリングしゆる～くコミット検知 - Qiita
	https://qiita.com/khsk/items/f559c798a8511cf118f4

	SE Life Log - VBAを中心にその他IT備忘録 -
	VBScript:VBScriptでDOSのコマンドを実行するサンプルプログラム | SE Life Log - VBAを中心にその他IT備忘録 -
	https://selifelog.com/blog-entry-171.html

	toshyon
	toshyon's memo
	https://yonetoshi.net/memo/1109/

	svnbook-dev@red-bean.com
	Subversionによるバージョン管理
	https://svnbook.red-bean.com/


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
